package com.jsp.core.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jsp.core.dto.ReadRequestDto;
import com.jsp.core.util.Clause;
import com.jsp.core.util.Projections;

@Service
public class CoreService {

	public String createProjections(List<Projections> projection) {
		StringBuilder builder = new StringBuilder();
		if(projection.size()==0) builder.append(" *");
		else {
		for (int i=0;i<projection.size();i++) {
			builder.append(" "+projection.get(i).getFieldName());
			if(i<projection.size()-1) 
				builder.append(",");
		}
		}
		return builder.toString();
	}
	
	public String createClauses(Clause clause) {
		StringBuilder builder = new StringBuilder();
		if(clause.getPredicates().size()!=0) {
		for (int i=0;i<clause.getPredicates().size();i++) {
			if(i==0) builder.append(" where "+clause.getPredicates().get(i).getDimension());
			else builder.append(" "+clause.getPredicates().get(i).getDimension());
			builder.append(" "+clause.getPredicates().get(i).getOperator()+" ");
			switch(clause.getPredicates().get(i).getOperator()) {
				case "in":
					for (int j=0;j<clause.getPredicates().get(i).getList().size();j++) {
						if(j==0) builder.append(" "+"(");
						if(clause.getPredicates().get(i).getList().get(j) instanceof String) {
							builder.append("'"+clause.getPredicates().get(i).getList().get(j)+"'");
						}
						else builder.append(clause.getPredicates().get(i).getList().get(j));
						if(j<clause.getPredicates().get(i).getList().size()-1) builder.append(",");
						if(j==clause.getPredicates().get(i).getList().size()-1) builder.append(")");
						
					}
					if(i<clause.getPredicates().size()-1) 
						builder.append(" "+clause.getType()+" ");
					break;
				case "=":
					if(clause.getPredicates().get(i).getValue() instanceof String)
						builder.append(" "+"'"+clause.getPredicates().get(i).getValue()+"'");
					else
						builder.append(" "+clause.getPredicates().get(i).getValue());
					if(i<clause.getPredicates().size()-1) 
						builder.append(" "+clause.getType()+" ");
					break;
				case "between":
					for(int j=0;j<clause.getPredicates().get(i).getRange().size();j++) {
						if(clause.getPredicates().get(i).getRange().get(j) instanceof String) {
							builder.append("'"+clause.getPredicates().get(i).getRange().get(j)+"'");
						}
						else builder.append(clause.getPredicates().get(i).getRange().get(j));
						if(j<clause.getPredicates().get(i).getRange().size()-1) builder.append(" AND ");
						
					}
					if(i<clause.getPredicates().size()-1) 
						builder.append(" "+clause.getType()+" ");
					break;
				
		}
		}
		return builder.toString();
		}
		else return new String();
	}
	
	public String formSqlQuery(ReadRequestDto readRequestDto) {
		StringBuilder builder = new StringBuilder();
		if(createClauses(readRequestDto.getFilter().getClauses().get(0)).length()!=0)
			builder.append("select"+createProjections(readRequestDto.getProjections())+" from "+readRequestDto.getEntityName()+createClauses(readRequestDto.getFilter().getClauses().get(0)));
		else
			builder.append("select"+createProjections(readRequestDto.getProjections())+" from "+readRequestDto.getEntityName()+" order by "+readRequestDto.getProjections().get(0).getFieldName()+" "+readRequestDto.getOrderBy());
			
	return builder.toString();
	}
	
}
