package com.jsp.core.util;

import java.util.List;

public class Predicates {

	private String dimension;
	private String operator;
	private Object value;
	private List<Object> range;
	private List<Object> list;
	public String getDimension() {
		return dimension;
	}
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public List<Object> getRange() {
		return range;
	}
	public void setRange(List<Object> range) {
		this.range = range;
	}
	public List<Object> getList() {
		return list;
	}
	public void setList(List<Object> list) {
		this.list = list;
	}
	
	
}
