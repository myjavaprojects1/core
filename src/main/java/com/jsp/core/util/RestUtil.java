package com.jsp.core.util;

import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestUtil {
	
	public static Map<String,Object> invokeApi(String url,HttpMethod type,Object requestBody,RestProperties restProperties,RestTemplate restTemplate){
		ResponseEntity<RestProperties> responseEntity = restTemplate.exchange(url, type, null,RestProperties.class);
		RestProperties properties = responseEntity.getBody();
		ObjectMapper objectMapper = new ObjectMapper();

		Map<String, Object> value = objectMapper.convertValue(properties,new TypeReference<Map<String,Object>>() {});
		
		return value;
	}
}
