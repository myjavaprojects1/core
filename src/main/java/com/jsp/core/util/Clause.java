package com.jsp.core.util;

import java.util.List;

public class Clause {

	private String type;
	private List<Predicates> predicates;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Predicates> getPredicates() {
		return predicates;
	}
	public void setPredicates(List<Predicates> predicates) {
		this.predicates = predicates;
	}
	
	
}
