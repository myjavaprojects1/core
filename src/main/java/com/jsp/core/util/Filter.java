package com.jsp.core.util;

import java.util.List;

public class Filter {

	private List<Clause> clauses;

	public List<Clause> getClauses() {
		return clauses;
	}

	public void setClauses(List<Clause> clauses) {
		this.clauses = clauses;
	}
}
