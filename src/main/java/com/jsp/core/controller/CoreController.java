package com.jsp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jsp.core.dto.ReadRequestDto;
import com.jsp.core.service.CoreService;

@RestController
public class CoreController {

	@Autowired
	private CoreService coreService;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@PostMapping(value="/read")
	public @ResponseBody String read(@RequestBody ReadRequestDto readRequestDto) {
		String sqlQuery = coreService.formSqlQuery(readRequestDto);
		return sqlQuery;
	}
}
